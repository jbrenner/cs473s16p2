#comp473proj2 Facilities.db DDL
#!/bin/sh

sqlite3 -batch Facilities.db <<EOF
CREATE TABLE user(
	idNumber Int Not Null,
	name char(50) Not Null,
	maintenance_request char(200),
	days_needed int,
	primary key(idNumber)
);
EOF
