package com.cs473.facilities;

/**
 * Created by Brenner on 4/4/2016.
 */
import java.util.List;

import com.cs473.facilities.DAO.FacilityDAO;
import com.cs473.facilities.DAO.UserDAO;
import com.cs473.facilities.model.Facility;
import com.cs473.facilities.model.Room;
import com.cs473.facilities.model.User;
import org.springframework.cache.support.NullValue;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.expression.spel.ast.NullLiteral;


public class main {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");

        UserDAO personDAO = context.getBean(UserDAO.class);

        User person = new User();
        person.setName("Jack"); person.setDays_needed(666); person.setUsage("teaching"); person.setIdNumber(666);



        FacilityDAO roomDAO = context.getBean(FacilityDAO.class);
        Room room = new Room();
        room.setCapacity(100); room.setDetail("Classroom"); room.setID(23);
        //roomDAO.save(room);
        person.addRoom(room);


        personDAO.save(person);


        List<User> user_list = personDAO.list();

        System.out.println("Person::"+person);
        for(User p : user_list){
            System.out.println("Person List::"+p);
        }


        List<Facility> room_list = roomDAO.list();
        System.out.println("Room::"+room);
        for(Facility f : room_list){
            System.out.println("Room List::"+f);
        }



        //close resources
        context.close();
    }
}