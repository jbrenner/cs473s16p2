package com.cs473.facilities.service;

import com.cs473.facilities.DAO.UserDAO;
import com.cs473.facilities.model.Room;
import com.cs473.facilities.model.User;
import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Brenner on 4/4/2016.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userdao;

    public UserDAO getUserdao() {
        return userdao;
    }

    public void setUserdao(UserDAO userdao) {
        this.userdao = userdao;
    }


    @Override
    @Transactional
    public void save(User u) {
        userdao.save(u);
    }

    @Override
    @Transactional
    public List<User> list() {
        return userdao.list();
    }


    //New Shit

    private static SessionFactory factory;
  /*  public static void main(String[] args) {
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        UserServiceImpl ME = new UserServiceImpl();

      *//* Let us have one address object *//*
        Room room = addRoom(123,007,40,"Classroom");

      *//* Add employee records in the database *//*
        Integer empID1 = ME.addEmployee("Manoj", "Kumar", 4000, address);

      *//* Add another employee record in the database *//*
        Integer empID2 = ME.addEmployee("Dilip", "Kumar", 3000, address);

      *//* List down all the employees *//*
        ME.listEmployees();

      *//* Update employee's salary records *//*
        ME.updateEmployee(empID1, 5000);

      *//* Delete an employee from the database *//*
        ME.deleteEmployee(empID2);

      *//* List down all the employees *//*
        ME.listEmployees();

    }*/

    /* Method to add an address record in the database */
/*
    public static Room addRoom(int roomID, int userID, int capacity, String detail) {
        Session session = factory.openSession();
        Transaction tx = null;
        Integer addressID = null;
        Room room = null;
        try{
            tx = session.beginTransaction();
            room = new Room (roomID , capacity,detail, userID)
            roomID = (Integer) session.save(room);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return room;
    }

    */
/* Method to add an employee record in the database *//*

    public Integer addEmployee(String fname, String lname,
                               int salary, Address address){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer employeeID = null;
        try{
            tx = session.beginTransaction();
            Employee employee = new Employee(fname, lname, salary, address);
            employeeID = (Integer) session.save(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return employeeID;
    }

    */
/* Method to list all the employees detail *//*

    public void listEmployees( ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            List employees = session.createQuery("FROM Employee").list();
            for (Iterator iterator =
                 employees.iterator(); iterator.hasNext();){
                Employee employee = (Employee) iterator.next();
                System.out.print("First Name: " + employee.getFirstName());
                System.out.print("  Last Name: " + employee.getLastName());
                System.out.println("  Salary: " + employee.getSalary());
                Address add = employee.getAddress();
                System.out.println("Address ");
                System.out.println("\tStreet: " +  add.getStreet());
                System.out.println("\tCity: " + add.getCity());
                System.out.println("\tState: " + add.getState());
                System.out.println("\tZipcode: " + add.getZipcode());
            }
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    */
/* Method to update salary for an employee *//*

    public void updateEmployee(Integer EmployeeID, int salary ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Employee employee =
                    (Employee)session.get(Employee.class, EmployeeID);
            employee.setSalary( salary );
            session.update(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    */
/* Method to delete an employee from the records *//*

    public void deleteEmployee(Integer EmployeeID){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Employee employee =
                    (Employee)session.get(Employee.class, EmployeeID);
            session.delete(employee);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
*/


}
