package com.cs473.facilities.service;

import com.cs473.facilities.DAO.FacilityDAO;
import com.cs473.facilities.DAO.UserDAO;
import com.cs473.facilities.model.Facility;
import com.cs473.facilities.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Brenner on 4/5/2016.
 */
@Service
public class RoomServiceImpl implements FacilityService{
    @Autowired
    private FacilityDAO facilitydao;

    public FacilityDAO getFacilitydao() {
        return facilitydao;
    }

    public void setFacilitydao(FacilityDAO facilitydao) {
        this.facilitydao = facilitydao;
    }

    @Override
    @Transactional
    public void save(Facility room) {
        facilitydao.save(room);
    }

    @Override
    @Transactional
    public List<Facility> list() {
        return facilitydao.list();
    }
}
