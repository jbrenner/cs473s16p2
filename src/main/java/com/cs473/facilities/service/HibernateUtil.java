package com.cs473.facilities.service;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.io.File;

/**
 * Created by Brenner on 4/24/2016.
 */
public class HibernateUtil {


    private static SessionFactory sessionFactory;



    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutDown() {
        //closes caches and connections
        getSessionFactory().close();

    }
    static {
        try {


            String hibernatePropsFilePath = "D:\\Documents\\School\\Spring16\\comp473s16p2\\cs473s16p2\\src\\main\\resources\\hibernate.cfg.xml";

            File hibernatePropsFile = new File(hibernatePropsFilePath);
            Configuration configuration = new Configuration(); configuration.configure(hibernatePropsFile);

            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            sessionFactory = configuration.buildSessionFactory(builder.build());
        } catch (HibernateException ex) {
            System.err.println("Initial sessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
