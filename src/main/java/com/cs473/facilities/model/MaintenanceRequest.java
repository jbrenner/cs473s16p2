package com.cs473.facilities.model;

/**
 * Created by Brenner on 4/7/2016.
 */
public interface MaintenanceRequest {
    public int getRequester(); // mandatory one to optional manyRelationship
    public String getRequestDetails();
    public int getDaysNeeded();
    public int getFacilityID();
    //getStartDate();

}
