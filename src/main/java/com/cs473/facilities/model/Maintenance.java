package com.cs473.facilities.model;

import java.util.List;

/**
 * Created by Brenner on 4/7/2016.
 */
public interface Maintenance {
    public List<MaintenanceRequest> maintenanceRequestList(); //optional many-to-one mapping
    public User getMaintenanceManager(); // mandatory one to optional many mapping
    public MaintenanceRequest getActiveMaintenance(); //optional one to one relationship

}
