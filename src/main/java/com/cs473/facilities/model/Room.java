package com.cs473.facilities.model;

import org.springframework.cache.support.NullValue;

import javax.persistence.*;

/**
 * Created by Brenner on 4/5/2016.
 */
@Entity
public class Room implements Facility {

    @Id
    private int roomID;
    private int userID;
    private int capacity;
    private String detail;

    public Room() {
    }

    public int getID() {
        return roomID;
    }

    public void setID(int roomID) {
        this.roomID = roomID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int user) {
        this.userID = user;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }





    @Override
    public String toString() {
        return "Room{" +
                "roomID=" + roomID +
                ", user=" + userID +
                ", capacity=" + capacity +
                ", detail='" + detail + '\'' +
                '}';
    }
}
