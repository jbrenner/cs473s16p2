package com.cs473.facilities.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Brenner on 4/4/2016.
 */

public class User {


  
    private int userID;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    private String name;
    private String usage;
    private int days_needed;
  
   private Set<Room> rooms = new HashSet();

  
    private Set<Building> buildings = new HashSet();

    public Set<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(Set<Building> buildings) {
        this.buildings = buildings;
    }

    public void addBuilding(Building b ){
        b.setUserID(this.userID);
        this.buildings.add(b);
    }

    public Set<Room> getRooms() {return rooms;}
   /* public void setRooms(Set<Room> rooms) { // refactor using hashset
        for( int i = 0; i< rooms.size(); i++) {
            Room room = rooms.get(i);
            room.setUserID(this.userID); // setUserID for each room
            rooms.set(i, room);
        }
        this.rooms = rooms;
    }*/
    public void addRoom(Room r){
        r.setUserID(this.userID);
        this.rooms.add(r);
    }
    public User(){}

    public void setIdNumber(int userID) {
        this.userID = userID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public void setDays_needed(int days_needed) {
        this.days_needed = days_needed;
    }

    public int getIdNumber() {return userID;}

    public String getName() {
        return name;
    }

    public int getDays_needed() {
        return days_needed;
    }

    public String getUsage() {return usage;}


    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                ", name='" + name + '\'' +
                ", usage='" + usage + '\'' +
                ", days_needed=" + days_needed +
                '}';
    }




}
