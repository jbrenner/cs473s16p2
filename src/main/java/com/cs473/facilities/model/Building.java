
package com.cs473.facilities.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by Brenner on 4/5/2016.
 */


public class Building implements Facility {

    int buildingID;

    public int getBuildingID() {
        return buildingID;
    }

    public void setBuildingID(int buildingID) {
        this.buildingID = buildingID;
    }

    private int userID;
    private int capacity;
    private String detail;

    private Set rooms;

    public Set getRooms() {
        return rooms;
    }

    public void setRooms(Set rooms) {
        this.rooms = rooms;
    }

    public int getID() {
        return buildingID;
    }

    public void setID(int buildingID) {
        this.buildingID = buildingID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String getDetail() {
        return detail;
    }

    @Override
    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Building(){};
}


