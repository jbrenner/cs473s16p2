package com.cs473.facilities.model;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Brenner on 4/7/2016.
 */
public class BuildingMaintenanceImpl implements Maintenance {
    public int maintenanceManager;
    public int activeMaintenanceRequestID;
    public int roomID;
    private String details;

    public void setMaintenanceManager(int maintenanceManager) {
        this.maintenanceManager = maintenanceManager;
    }

    public int getActiveMaintenanceRequestID() {
        return activeMaintenanceRequestID;
    }

    public void setActiveMaintenanceRequestID(int activeMaintenanceRequestID) {
        this.activeMaintenanceRequestID = activeMaintenanceRequestID;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @OneToMany(fetch = FetchType.LAZY,  cascade = CascadeType.ALL)
    @JoinColumn(name = "buildingID")
    private Set<MaintenanceRequest> requests = new HashSet();
    public void addMaintenanceRequest(MaintenanceRequestImpl b ){
        b.setFacilityID(this.roomID);
        this.requests.add(b);
    }

    @Override
    public List<MaintenanceRequest> maintenanceRequestList() {
        return null;
    }

    @Override
    public User getMaintenanceManager() {
        return null;
    }

    @Override
    public MaintenanceRequest getActiveMaintenance() {
        return null;
    }
}
