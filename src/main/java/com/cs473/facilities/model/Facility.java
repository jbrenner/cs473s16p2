package com.cs473.facilities.model;

/**
 * Created by Brenner on 4/5/2016.
 */
public interface Facility {

    public int getUserID();

    public void setUserID(int user);

    public int getCapacity();

    public void setCapacity(int capacity);

    public String getDetail();

    public void setDetail(String detail);

    public int getID();

    public void setID(int ID) ;

}
