package com.cs473.facilities.model;

/**
 * Created by Brenner on 4/7/2016.
 */
public class MaintenanceRequestImpl implements MaintenanceRequest {
    private int maintenanceRequestID;
    private int userID;
    private String requestDetails;
    private int daysNeeded;
    private int facilityID;

    public void setMaintenanceRequestID(int maintenanceRequestID) {
        this.maintenanceRequestID = maintenanceRequestID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setRequestDetails(String requestDetails) {
        this.requestDetails = requestDetails;
    }

    public void setDaysNeeded(int daysNeeded) {
        this.daysNeeded = daysNeeded;
    }

    public void setFacilityID(int facilityID) {
        this.facilityID = facilityID;
    }

    public MaintenanceRequestImpl() {
    }

    @Override
    public int getRequester() {
        return this.userID;
    }

    @Override
    public String getRequestDetails() {
        return this.requestDetails;
    }

    @Override
    public int getDaysNeeded() {
        return this.daysNeeded;
    }

    @Override
    public int getFacilityID() {
        return this.facilityID;
    }
}
