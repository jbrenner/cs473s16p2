package com.cs473.facilities;

/**
 * Created by Brenner on 4/24/2016.
 */
import com.cs473.facilities.model.Building;
import com.cs473.facilities.model.Room;
import com.cs473.facilities.model.User;
import org.hibernate.Session;
import com.cs473.facilities.service.HibernateUtil;

public class App {
    public static void main(String[] args) {
        System.out.println("Hibernate one to one (XML mapping)");
        Session session = HibernateUtil.getSessionFactory().openSession();

        session.beginTransaction();

        User user = new User();

        //set user fields
        user.setIdNumber(123);
        user.setUsage("Owner");
        user.setDays_needed(10);
        user.setName("Bob");

        Building building = new Building();
        //set building fields

        session.save(user);
        session.getTransaction().commit();

        System.out.println("Done");
    }
}

