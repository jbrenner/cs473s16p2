package com.cs473.facilities.DAO;

import com.cs473.facilities.model.Facility;
import com.cs473.facilities.model.Room;
import com.cs473.facilities.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Brenner on 4/5/2016.
 */
@Repository
public class RoomDAOImpl implements FacilityDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    @Override
    public void save(Facility room) {
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(room);
        tx.commit();
        session.close();
    }
    @SuppressWarnings("unchecked")
    @Override
    public List<Facility> list() {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        List<Facility> roomList = session.createQuery("from Room").list(); // IN HQL use class and property name rather than table and col
        session.close();
        return roomList;
    }
}
