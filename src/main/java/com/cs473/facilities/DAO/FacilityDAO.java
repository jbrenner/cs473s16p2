package com.cs473.facilities.DAO;

import com.cs473.facilities.model.Facility;

import java.util.List;

/**
 * Created by Brenner on 4/4/2016.
 */
public interface FacilityDAO  {
    public void save(Facility f);
    public List<Facility> list();
}
