package com.cs473.facilities.DAO;

import com.cs473.facilities.model.User;

import java.util.List;

/**
 * Created by Brenner on 4/4/2016.
 */
public interface UserDAO {
    public void save(User u);
    public List<User> list();

}
